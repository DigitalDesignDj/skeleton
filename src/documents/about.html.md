---
layout: 'default'
title: 'About'
header: 'About'
footer: ''
---
This is another page, where you can do some stuff.

To add a new page, create a new file like {filename}.html.md extension. You will then need to add the new page to the menu at `src/layouts/menu.json`. Use {filename}.html as the link. You can use folders and nesting to organize things if you wish, but the menu is only one layer right now.
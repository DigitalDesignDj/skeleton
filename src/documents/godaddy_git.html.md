---
layout: 'default'
title: 'GoDaddy git'
header: 'Installing git on GoDaddy hosting'
footer: 'just some links for now, gotta figure out SSH'
---

Just some links for now

- [Helpful Guide](http://dren.ch/git-on-godaddy/)
- [Git Package](http://pkgs.repoforge.org/git/)
- [Tip about `mkdir` and SSH](http://support.godaddy.com/groups/web-hosting/forum/topic/ssh-public-keys/?pc_split_value=3)